/**
 * Created by Tom on 13/10/2015.
 */
import java.net._
import java.io._
import scala.io._

class Client(port: Int, i:Int) extends Runnable {

  val socket = new Socket(InetAddress.getByName("localhost"), port)
  def run() {

    val out = new PrintStream(socket.getOutputStream())
    println("This is a test in thread :"+ i)
    (i)match{
      case 0 =>out.println("HELO text\n")
      case 1 =>out.println("HELOtext\n")
      case 2 =>out.println("HELO Nobody is King\n")
      case 3 =>out.println("Helo Hail Satan \n")
      case 4 =>out.println("\n")
      case 5 =>out.println("Kill_Service")
      case 6 =>out.println("KILL_SERVICE\n")
    }
    out.flush()


    println("This is working")
    socket.close()
  }

}

