/**
 * Created by Tom on 27/10/2015.
 */
object TestClient {
  def main(args: Array[String]) {
    val noOfThreads = 6
    for(i <- 0 to noOfThreads) {
      val cli = new Client (800,i)
      cli.run()
    }
  }
}
