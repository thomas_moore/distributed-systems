/**
 * Created by Tom on 20/10/2015.
 */
object ServerRun {
  def main(args: Array[String]) {
    var port = args(0).toInt
    val serv = new Server(port)
    serv.run()
  }
}
